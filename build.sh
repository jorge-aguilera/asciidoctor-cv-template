mkdir build
cp -R stylesheets build

asciidoctor -D build -o index.html template.adoc
asciidoctor-pdf -D build -o template.pdf template.adoc
